(function ($, Drupal) {
  'use strict';
  
  Drupal.behaviors.badges_async = {
    attach: function (context, settings) {
      var $selector = $('.badge-async-placeholder', context);
      
      if (!$selector.length) {
        return false;
      }
      
      var badges = {};
      
      $selector.each(function () {
        var id = $(this).data('id');
        badges[id] = {
          "id": id,
          "plugin-name": $(this).data('plugin-name'),
          "attributes": $(this).data('attributes'),
        };
      });
      
      if (badges) {
        $.ajax({
          type: 'POST',
          url: '/badges-async/get/json',
          data: JSON.stringify({"badges": badges}),
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          success: function (response) {
            
            if (!response.data.badges) {
              return false;
            }
            
            $.each(response.data.badges, function (id, markup) {
              $('.badge-async-placeholder[data-id="'+ id +'"]').html(markup);
            });
          },
          error: function (error) {
            console.log(error);
          }
        });
      }
    }
  };
  
}(jQuery, Drupal));
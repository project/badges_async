<?php

namespace Drupal\badges_async\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Badges async plugins.
 */
interface BadgesAsyncInterface extends PluginInspectionInterface {

  public function badgeResult(string $badge_id, string $attributes);

}

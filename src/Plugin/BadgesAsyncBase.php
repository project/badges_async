<?php

namespace Drupal\badges_async\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Badges async plugins.
 */
abstract class BadgesAsyncBase extends PluginBase implements BadgesAsyncInterface {

  protected $entityTypeManager;

}

<?php

namespace Drupal\badges_async\Plugin\BadgesAsync;

use Drupal\badges_async\Annotation\BadgesAsync;
use Drupal\badges_async\Plugin\BadgesAsyncBase;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Plugin implementation of the 'node_is_new' badge async.
 *
 * @BadgesAsync(
 *   id = "node_is_new",
 *   label = @Translation("Node is new")
 * )
 */
class NodeIsNew extends BadgesAsyncBase {

  private static $cache = [];

  protected $entityTypeManager;

  public function badgeResult(string $badge_id, string $attributes) {

    if (!$status = $this->getNodeStatus($attributes)) {
      return '';
    }

    /** @var \Drupal\Core\Render\Renderer $renderer */
    $renderer = \Drupal::service('renderer');
    $element = [
      '#theme' => 'badges_async',
      '#content' => $status,
    ];

    return $renderer->render($element);
  }

  protected function getNodeStatus($nid) {

    if (!$node = Node::load($nid)) {
      return '';
    }

    $display = ['new' => 1, 'updated' => 0]; // @todo : get from config
    $time_limit = $this->getTimeLimit();
    $nid = $node->id();

    if (!isset(static::$cache[$nid])) {
      static::$cache[$nid] = $this->getNodeLastViewed($nid);
    }

    if ($display['new'] && static::$cache[$nid] == 0 && $node->getCreatedTime() > $time_limit) {
      return t('new');
    }
    elseif ($display['updated'] && $node->getChangedTime() > static::$cache[$nid] && $node->getChangedTime() > $time_limit) {
      return t('updated');
    }

    return '';
  }

  protected function getTimeLimit() {
    $days_limits = 7; // @todo : get from config
    $timestamp = strtotime('-' . $days_limits . ' day', \Drupal::time()->getRequestTime());
    return $timestamp;
  }

  protected function getNodeLastViewed($nid) {
    $database = \Drupal::database(); // @todo : handle with dependency injection
    $uid = \Drupal::currentUser()->id(); // @todo : handle with dependency injection

    $query = $database->query("SELECT timestamp FROM {history} WHERE uid = :uid AND nid = :nid", [
      ':uid' => $uid,
      ':nid' => $nid,
    ]);

    $history = $query->fetchObject();

    return $history->timestamp ?? 0;
  }

}

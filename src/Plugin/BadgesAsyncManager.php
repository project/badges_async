<?php

namespace Drupal\badges_async\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Badges async plugin manager.
 */
class BadgesAsyncManager extends DefaultPluginManager {


  /**
   * Constructs a new BadgesAsyncManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/BadgesAsync', $namespaces, $module_handler, 'Drupal\badges_async\Plugin\BadgesAsyncInterface', 'Drupal\badges_async\Annotation\BadgesAsync');

    $this->alterInfo('badges_async_badges_async_info');
    $this->setCacheBackend($cache_backend, 'badges_async_badges_async_plugins');
  }

}

<?php

namespace Drupal\badges_async\Controller;

use Drupal\badges_async\Plugin\BadgesAsyncBase;
use Drupal\badges_async\Plugin\BadgesAsyncManager;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * BadgesAsync controller.
 */
class BadgesAsyncController extends ControllerBase {

  protected $badgesAsyncManager;
  protected $entityTypeManager;

  public function __construct(BadgesAsyncManager $badgesAsyncManager, EntityTypeManagerInterface $entityTypeManager) {
    $this->badgesAsyncManager = $badgesAsyncManager;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.badges_async'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Returns json response
   */
  public function json(Request $request) {
    $content = json_decode($request->getContent(), TRUE);
    $plugins = [];
    $badges = [];

    // Fetch badge plugins
    foreach ($content['badges'] as $id => $badge) {

      if (!empty($plugins[$badge['plugin-name']])) {
        /** @var BadgesAsyncBase $plugin */
        $plugin = $plugins[$badge['plugin-name']];
      } else {
        /** @var BadgesAsyncBase $plugin */
        $plugin = $this->badgesAsyncManager->createInstance($badge['plugin-name']);
        $plugins[$badge['plugin-name']] = $plugin;
      }

      $badges[$id] = $plugin->badgeResult($id, $badge['attributes']);
    }

    return new JsonResponse([
      'status' => 200,
      'message' => 'success',
      'data' => [
        'badges' => $badges
      ],
    ]);
  }

}

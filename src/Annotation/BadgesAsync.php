<?php

namespace Drupal\badges_async\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Badges async item annotation object.
 *
 * @see \Drupal\badges_async\Plugin\BadgesAsyncManager
 * @see plugin_api
 *
 * @Annotation
 */
class BadgesAsync extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
